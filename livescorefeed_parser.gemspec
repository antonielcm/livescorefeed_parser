# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'livescorefeed_parser/version'

Gem::Specification.new do |spec|
  spec.name          = "livescorefeed_parser"
  spec.version       = LivescorefeedParser::VERSION
  spec.authors       = ["Pedro Calgaro"]
  spec.email         = ["pedro.e.calgaro@gmail.com"]
  spec.description   = %q{Gem to parse scores from LiveScoreFeed}
  spec.summary       = %q{This gem is a JSON parser for LiveScoreFeed results}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_runtime_dependency "httparty"
  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end
